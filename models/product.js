const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productSchema = Schema({
  productCode: {
    type: String,
    required: true,
    unique: true,
  },
  title: {
    type: String,
    required: false,
  },
  imagePath: {
    type: String,
    required: false,
  },
  description: {
    type: String,
    required: false,
  },
  price: {
    type: Number,
    required: true,
    default: 0
  },
  point: {
    type: Number,
    required: true,
    default: 0
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Category",
  },
  manufacturer: {
    type: String,
  },
  available: {
    type: Boolean,
    required: false,
  },
  usePoint: {
    type: Boolean,
    required: true,
    default: false
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  uid: {
    type: String,
    default: ""
  },
  pass: {
    type: String,
    default: ""
  },
  token: {
    type: String,
    default: ""
  },
  cookie: {
    type: String,
    default: ""
  },
  _2fa: {
    type: String,
    default: ""
  },
  email: {
    type: String,
    default: ""
  },
  passEmail: {
    type: String,
    default: ""
  },
  birthday: {
    type: String,
    default: ""
  },
  brand: {
    type: String,
    default: ""
  },
  proxy: {
    type: String,
    default: ""
  },
});

module.exports = mongoose.model("Product", productSchema);
